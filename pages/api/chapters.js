export default async function handler(req, res) {
  console.log(`${process.env.MONGO_KOANS_URL}/chapter`);
  try {
    const response = await fetch(`${process.env.MONGO_KOANS_URL}/chapter`);
    console.log(response);
    if (response.ok) {
      const data = await response.json();
      res.status(200).json(data);
    } else {
      res
        .status(400)
        .text("problem fetching " + `${process.env.MONGO_KOANS_URL}/chapter`);
    }
  } catch (ex) {
    console.error(ex);
    res.status(400).json(ex);
  }
}
